/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Strasbourg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Pierre Weiss <3weissp@gmail.com>
 */

/**
 * \file node.cpp
 * \brief Node base class.
 * \author Pierre Weiss
 * \date 2009
 */

#include "node.h"
#include "generator.h"

#include "utils.h"

Node::Node(const size_t &indice, const std::string &type, const std::string &namePrefix, const size_t &machinesNumber)
{
  this->IFACE_NUMBER = 256;
  this->m_indice = indice;
  this->m_type = type;
  this->m_nodeName = std::string(namePrefix + utils::integerToString(indice));
  this->m_ipInterfaceName = std::string("iface_" + this->m_nodeName);
  this->m_ipInterfaceNumber = 0;
  this->m_nsc = std::string("");
  this->m_template = std::string(""); // CyberVan
  this->m_img = std::string(""); // CyberVan
  this->m_imgType = std::string("pvm"); // CyberVan
  this->m_mac = std::string(""); // CyberVan
  this->m_cpu = 0; // CyberVan
  this->m_mem = 0; // CyberVan
  this->m_l2flags = new bool[256](); 
  this->m_machinesNumber = machinesNumber;
  this->m_indiceRoute = 0; 
}

Node::~Node()
{
}

void Node::SetNodeName(const std::string &nodeName)
{
  this->m_nodeName = nodeName;
}

void Node::SetIpInterfaceName(const std::string &ipInterfaceName)
{
  this->m_ipInterfaceName = ipInterfaceName;
}

void Node::SetIpInterfaceNumber(const size_t &number)
{
  this->m_ipInterfaceNumber = number;
}

size_t Node::GetIpInterfaceNumber()
{
  return this->m_ipInterfaceNumber;
}

void Node::SetL2Flag(const size_t &index)
{
  if (index > IFACE_NUMBER) return;
  this->m_l2flags[index] = true;
}

void Node::UnsetL2Flag(const size_t &index)
{
  if (index > IFACE_NUMBER) return;
  this->m_l2flags[index] = false;
}

bool Node::IsSetL2Flag(const size_t &index)
{
  if (index > IFACE_NUMBER)
    return false;
  return this->m_l2flags[index];
}

std::string Node::GetNodeName()
{
  return this->m_nodeName;
}

std::string Node::GetNodeName(const size_t &number)
{
  return std::string("NodeContainer(" + this->m_nodeName + ".Get(" + utils::integerToString(number) + "))");
}

std::string Node::GetIpInterfaceName()
{
  return this->m_ipInterfaceName;
}

std::string Node::GetIndice()
{
  return utils::integerToString(this->m_indice);
}

void Node::SetIndice(const size_t &indice)
{
  this->m_indice = indice;
}

void Node::SetNsc(const std::string &nsc)
{
  this->m_nsc = nsc;
}

std::string Node::GetTemplate()
{
  return this->m_template;
}

void Node::SetTemplate(const std::string &tem)
{
  this->m_template = tem;
}

std::string Node::GetImg()
{
  return this->m_img;
}

void Node::SetImg(const std::string &img)
{
  this->m_img = img;
}

std::string Node::GetImgType()
{
  return this->m_imgType;
}

void Node::SetImgType(const std::string &imgType)
{
  this->m_imgType = imgType;
}

std::string Node::GetMac(const size_t &index)
{
  if (index >= this->m_macStack.size()) return "";
  else return this->m_macStack.at(index);
}

void Node::SetMac(const size_t &index, const std::string &mac)
{
  if (index > 255) return;

  size_t buffer = 5; // buffer to growth time
  if (index >= this->m_macStack.size()) {
    while (index + buffer > this->m_macStack.size()) 
      this->m_macStack.push_back("");
  }
  
  this->m_macStack.at(index) = mac;
}

std::string Node::GetIP(const size_t &index)
{
  if (index >= this->m_ipStack.size()) return "";
  else return this->m_ipStack.at(index);
}

void Node::SetIP(const size_t &index, const std::string &ip)
{
  if (index > 255) return;

  size_t buffer = 5; // buffer to growth time
  if (index >= this->m_ipStack.size()) {
    while (index + buffer > this->m_ipStack.size()) 
      this->m_ipStack.push_back("");
  }
  
  this->m_ipStack.at(index) = ip;
}

std::string Node::GetMask(const size_t &index)
{
  if (index >= this->m_maskStack.size()) return "";
  else return this->m_maskStack.at(index);
}

void Node::SetMask(const size_t &index, const size_t &mask)
{
  if (index > 255) return;

  size_t buffer = 5; // buffer to growth time
  if (index >= this->m_maskStack.size()) {
    while (index + buffer > this->m_maskStack.size()) 
      this->m_maskStack.push_back("");
  }
  
  this->m_maskStack.at(index) = utils::integerToString(mask);
}

std::string Node::GetCPU()
{
  return utils::integerToString(this->m_cpu);
}

void Node::SetCPU(const size_t &cpu)
{
  this->m_cpu = cpu;
}

std::string Node::GetMem()
{
  return utils::integerToString(this->m_mem);
}

void Node::SetMem(const size_t &Mem)
{
  this->m_mem = Mem;
}

std::string Node::GetNsc()
{
  return this->m_nsc;
}

size_t Node::GetMachinesNumber()
{
  return this->m_machinesNumber;
}

void Node::SetMachinesNumber(const size_t machinesNumber)
{
  this->m_machinesNumber = machinesNumber;
}

std::vector<std::string> Node::GenerateHeader()
{
  std::vector<std::string> headers;
  headers.push_back("#include \"ns3/ipv4-global-routing-helper.h\"");

  return headers; 
}

std::vector<std::string> Node::GenerateNodeCpp()
{
  std::vector<std::string> nodes;
  nodes.push_back("NodeContainer " + this->m_nodeName + ";");
  nodes.push_back(this->m_nodeName + ".Create (" + utils::integerToString(this->m_machinesNumber) + ");");

  return nodes; 
}

std::vector<std::string> Node::GenerateNodeCyberVan()
{
  std::vector<std::string> nodes;
  std::string cpu = "", mem = "";
  if (this->m_cpu > 0)
    cpu = " cpu=\""+ utils::integerToString(this->m_cpu) +"\" ";
  if (this->m_mem > 0)
    mem = " memory=\""+ utils::integerToString(this->m_mem) +"\" ";

  nodes.push_back("<node template=\""+ this->m_template + "\" name=\""+ this->m_nodeName +"\""+ cpu + mem +">");
  // if (this->m_img != "")
  //   nodes.push_back("  <base_image type=\"pvm\">"+ this->m_img +"</base_image>");
  return nodes; 
}

std::vector<std::string> Node::GenerateIpStackCpp()
{
  std::vector<std::string> stack;
  
  if(this->m_nsc != "")
  {
    stack.push_back("internetStackH.SetTcp (\"ns3::NscTcpL4Protocol\",\"Library\",StringValue(nscStack));");
  }
  stack.push_back("internetStackH.Install (" + this->m_nodeName + ");");

  return stack; 
}

std::vector<std::string> Node::GenerateNodePython()
{
  std::vector<std::string> nodes;
  
  nodes.push_back(this->m_nodeName + " = ns3.NodeContainer()");
  nodes.push_back(this->m_nodeName + ".Create (" + utils::integerToString(this->m_machinesNumber) + ")");
  return nodes; 
}

std::vector<std::string> Node::GenerateIpStackPython()
{
  std::vector<std::string> stack;
  
  if(this->m_nsc != "")
  {
    stack.push_back("internetStackH.SetTcp (\"ns3::NscTcpL4Protocol\",\"Library\",StringValue(nscStack))");
  }
  stack.push_back("internetStackH.Install (" + this->m_nodeName + ")");

  return stack; 
}

std::string Node::GetNodeType()
{
  return this->m_type;
}

void Node::AddRoute(const std::string &dest, const std::string &mask, const std::string &gateway, const int &iFaceIndex)
{
  Route *tmp;
  if (gateway != "")
    tmp = new Route(this->m_indiceRoute, mask, dest, gateway);
  else
    tmp = new Route(this->m_indiceRoute, mask, dest, iFaceIndex);
  this->m_indiceRoute += 1;
  this->m_listRoute.push_back(tmp);
}

void Node::RemoveRoute(const size_t &index)
{
  size_t startNumber = this->m_listRoute.size();

  delete this->m_listRoute[index];
  this->m_listRoute.erase(this->m_listRoute.begin() + index);

  size_t endNumber = this->m_listRoute.size();
  if(startNumber == endNumber)
  {
    throw std::logic_error("Route remove failed! (" + utils::integerToString(index) + ") not found.");
  }
}

Route* Node::GetRoute(const size_t &index)
{
  if(this->m_listRoute.size() < index)
  {
    throw std::out_of_range("Index does not exist");
    return 0;
  }

  return this->m_listRoute.at(index);
}

size_t Node::GetNRoutes() const
{
  return this->m_listRoute.size();
}

