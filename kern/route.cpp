/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Strasbourg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Pierre Weiss <3weissp@gmail.com>
 */

/**
 * \file Route.cpp
 * \brief Route base class.
 * \author Pierre Weiss
 * \date 2009
 */

#include "route.h"
#include "generator.h"

#include "utils.h"

Route::Route(const size_t &index, const std::string &mask, const std::string &dest, const std::string &gateway)
{
  this->m_rIndex = index;
  this->m_dest = dest;
  this->m_mask = mask;
  this->m_gateway = gateway;
}

Route::Route(const size_t &index, const std::string &mask, const std::string &dest, const int &iFaceIndex)
{
  this->m_rIndex = index;
  this->m_dest = dest;
  this->m_mask = mask;
  this->m_iFace = iFaceIndex;
}

Route::~Route()
{
}

void Route::SetDestination(const std::string &dest)
{
  this->m_dest = dest;
}

std::string Route::GetDestination()
{
  return this->m_dest;
}

void Route::SetGateway(const std::string &gateway)
{
  this->m_gateway = gateway;
}

std::string Route::GetGateway()
{
  return this->m_gateway;
}

void Route::SetIFace(const int &iFaceIndex)
{
  this->m_iFace = iFaceIndex;
}

int Route::GetIFace()
{
  return this->m_iFace;
}

void Route::SetMask(const std::string &mask)
{
  this->m_mask = mask;
}

std::string Route::GetMask()
{
  return this->m_mask;
}

