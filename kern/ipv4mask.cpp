/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; tab-width:2; -*- */

#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include "ipv4mask.h"

Ipv4Mask::Ipv4Mask(size_t mask) {
  m_mask = (uint32_t)(((1L << (32 - mask)) - 1) ^ 0xffffffff);
}

Ipv4Mask::Ipv4Mask(std::string mask) {
  int v[4];
  int sr = sscanf(mask.c_str(), "%d.%d.%d.%d", &v[0], &v[1], &v[2], &v[3]);
  if (sr == 4)
    m_mask = (v[3] & 0xff) | ((v[2] & 0xff) << 8) | ((v[1] & 0xff) << 16) | ((v[0] & 0xff) << 24);
  else
    m_mask = (uint32_t)(((1L << (32 - v[0])) - 1) ^ 0xffffffff);
}

std::string Ipv4Mask::GetStr() {
  std::ostringstream out;
  out << ((m_mask >> 24) & 0xff) << "." << ((m_mask >> 16) & 0xff) << "." << ((m_mask >> 8) & 0xff) << "." << (m_mask & 0xff);
  return out.str();
}

size_t Ipv4Mask::GetInt() {
  return !m_mask ? 0 : 32 - __builtin_ctz(m_mask);
}

std::string Ipv4Mask::Mask(std::string ip) {
  uint32_t ip_i;
  int v[4];
  sscanf(ip.c_str(), "%d.%d.%d.%d", &v[0], &v[1], &v[2], &v[3]);
  ip_i = (v[3] & 0xff) | ((v[2] & 0xff) << 8) | ((v[1] & 0xff) << 16) | ((v[0] & 0xff) << 24);
  uint32_t res = ip_i & m_mask;
  std::ostringstream out;
  out << ((res >> 24) & 0xff) << "." << ((res >> 16) & 0xff) << "." << ((res >> 8) & 0xff) << "." << (res & 0xff);
  return out.str();
}

std::string Ipv4Mask::PartialMask(std::string ip) {
  uint32_t ip_i;
  int v[4];
  sscanf(ip.c_str(), "%d.%d.%d.%d", &v[0], &v[1], &v[2], &v[3]);
  ip_i = (v[3] & 0xff) | ((v[2] & 0xff) << 8) | ((v[1] & 0xff) << 16) | ((v[0] & 0xff) << 24);
  uint32_t res = ip_i & m_mask;
  std::ostringstream out;
  int obits = 0, mbits = GetInt();
  while (obits < mbits) {
    if (obits) out << ".";
    out << ((res >> (24 - obits)) & 0xff);
    obits += 8;
  }
  return out.str();
}

