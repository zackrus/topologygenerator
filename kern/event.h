/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <@gmail.com>
 */

/**
 * \file event.h
 * \brief The event class is subclass of Application.
 * \author Juan Sepulveda
 * \date 2016
 */

#ifndef EVENT_H
#define EVENT_H

#include "application.h"

#include <iostream>
#include <string>
#include <vector>

/**
 * \ingroup generator
 * \brief The Event subclass from Application.
 *
 *  The Event class is a subclass of application.
 *
 *  This class represent a cybervan event application that details events in the node object of an xml file.
 *  Start time of specified script.
 *
 *  It generates event xml code.
 *
 *  Usage:
 *    - Attach events to specific nodes.
 */
class Event : public Application
{
  private:
    /**
     * \brief Application Script.
     */
    std::string m_scriptName;

    /**
     * \brief Determines whether script is run at stop time
     */
    bool m_endScript;

  public:
    /**
     * \brief Constructor.
     * \param type application type
     * \param indice indice in the generator vector
     * \param senderNode sender node
     * \param receiverNode receiver node
     * \param startTime application start time
     * \param endTime application end time
     */
    Event(const std::string &type, const size_t &indice, const std::string &senderNode, const std::string &scriptName, const int &startTime);

    /**
     * \brief Destructor.
     */
    ~Event();

    /**
     * \brief Generate the headers code.
     * \return headers code
     */
    virtual std::vector<std::string> GenerateHeader();

    /**
     * \brief Get Script Name
     * \return name of script
     */
    std::string GetScript();

    /**
     * \brief Set script name
     * \param name new name of script
     */
    void SetScript(const std::string &name);

    /**
     * \brief Get Script time as integer in the form of a string
     * \return start time
     */
    std::string GetTime();

    /**
     * \brief Set start time
     * \param name new name of script
     */
    void SetTime(const int &startTime);

    /**
     * \brief Generate the event xml code.
     * \return application code
     */
    virtual std::vector<std::string> GenerateApplicationCyberVan();

    /**
     * \brief Generate application C++ code.
     * \param netDeviceContainer net device container
     * \param numberIntoNetDevice number into net device
     * \return application code
     */
    virtual std::vector<std::string> GenerateApplicationCpp(std::string netDeviceContainer, size_t numberIntoNetDevice);

    /**
     * \brief Generate application python code.
     * \param netDeviceContainer net device container
     * \param numberIntoNetDevice number of the sender node into the net device container
     * \return the ns3 application code
     */
    virtual std::vector<std::string> GenerateApplicationPython(std::string netDeviceContainer, size_t numberIntoNetDevice);

    
};

#endif /* EVENT_H */

