/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Strasbourg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Pierre Weiss <3weissp@gmail.com>
 */

/**
 * \file template.cpp
 * \brief CyberVan Template base class.
 * \author Juan Sepulveda
 * \date 2016
 */

#include "template.h"
#include "event.h"

#include "utils.h"

Template::Template(const std::string &name, const size_t &indice, const std::string &img, const std::string &imgType, const size_t &cpu, const size_t &mem, const size_t &ipmask)
{
  this->m_name = name;
  this->m_indice = indice;
  this->m_img = img;
  this->m_imgType = imgType;
  this->m_cpu = cpu;
  this->m_mem = mem;
  this->m_indiceEvent = 0;
  this->m_ipmask = ipmask;
}

Template::~Template()
{
}

std::string Template::GetIndice()
{
  return utils::integerToString(this->m_indice);
}

void Template::SetIndice(const size_t &indice)
{
  this->m_indice = indice;
}

std::string Template::GetTempName()
{
  return this->m_name;
}

void Template::SetTempName(const std::string &name)
{
  this->m_name = name;
}

std::string Template::GetImg()
{
  return this->m_img;
}

void Template::SetImg(const std::string &img)
{
  this->m_img = img;
}

std::string Template::GetImgType()
{
  return this->m_imgType;
}

void Template::SetImgType(const std::string &imgType)
{
  this->m_imgType = imgType;
}

std::string Template::GetCPU()
{
  return utils::integerToString(this->m_cpu);
}

void Template::SetCPU(const size_t &cpu)
{
  this->m_cpu = cpu;
}

std::string Template::GetMem()
{
  return utils::integerToString(this->m_mem);
}

void Template::SetMem(const size_t &mem)
{
  this->m_mem = mem;
}

std::string Template::GetIPMask()
{
  return utils::integerToString(this->m_ipmask);
}

void Template::SetIPMask(const size_t &ipmask)
{
  this->m_ipmask = ipmask;
}

size_t Template::GetNEvent()
{
  return this->m_indiceEvent;
}

Event* Template::GetEvent(size_t index)
{
  if(this->m_events.size() < index)
  {
    throw std::out_of_range("Index does not exist");
    return 0;
  }

  return this->m_events.at(index);
}

void Template::NewEvent(const std::string &scriptName, const int &startTime)
{
  Event *eve = new Event("Event", this->m_indiceEvent, "null", scriptName, startTime);
  this->m_indiceEvent += 1;
  this->m_events.push_back(eve);
}

void Template::DeleteEvent(const size_t &index)
{
  delete this->m_events[index];
  this->m_events.erase(this->m_events.begin() + index);
  this->m_indiceEvent -= 1;
}

std::vector<std::string> Template::GenerateTemplateCyberVan()
{
  std::vector<std::string> temp;
  std::string cpu, memory, ipmask;
  if(this->m_cpu > 0)
    cpu = " cpu=\""+ utils::integerToString(this->m_cpu) +"\" ";
  if(this->m_mem > 0)
    memory = " memory=\""+ utils::integerToString(this->m_mem) +"\" ";
  ipmask = " ipmask=\""+ utils::integerToString(this->m_ipmask) +"\" ";
  temp.push_back("<node_template name=\""+ this->m_name +"\"" + cpu + memory + ipmask + ">");
  if (this->m_img != "") temp.push_back("  <base_image type=\""+ this->m_imgType +"\">"+ this->m_img +"</base_image>");
  for(size_t i = 0; i < this->m_indiceEvent; i++)
  {
    std::string startTime = this->m_events.at(i)->GetTime();
    std::string scriptName = this->m_events.at(i)->GetScript();
    if (startTime == "-1") startTime = "stop";
    if (startTime == "0") startTime = "start";
    if (i == 0) temp.push_back("  <events interpreter=\"/usr/local/bin/runevents\">");
    temp.push_back("    <event time=\""+ startTime +"\" script=\""+ scriptName +"\" />");
    if ( i == this->m_indiceEvent-1) temp.push_back("  </events>");
  }
  temp.push_back("</node_template>");
  return temp;
}
