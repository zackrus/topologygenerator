/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <github: juan->
 */

/**
 * \file simulation-pages.h
 * \brief Pages for CyberVan purposes
 * \author Juan Sepulveda
 * \date 2016
 */

#ifndef SIMULATION_PAGES_H
#define SIMULATION_PAGES_H

#include <QWidget>
#include <QObject>
#include "drag-widget.h"

/**
 * \brief Node Settings page
 */
class SettingsPage : public QWidget
{
  Q_OBJECT

  private:
    /**
     * \brief parent qwidget object
     */
    QWidget *m_parent;

    /**
     * \brief drag widget object
     */
    DragWidget *m_dw;
    
    /**
     * \brief drag widget object
     */
    DragObject *m_node;
    
    /**
     * \brief selected node index
     */
    size_t m_nodeIndex;
    
    /**
     * \brief selected node name
     */
    std::string m_nodeName;
    
    /**
     * \brief selected node template
     */
    std::string m_nodeTemp;
    
    /**
     * \brief selected node base image
     */
    std::string m_nodeImg;

    /**
     * \brief selected node base image type
     */
    std::string m_nodeImgType;

    /**
     * \brief selected node cpu number
     */
    std::string m_nodeCPU;

    /**
     * \brief selected node memory
     */
    std::string m_nodeMem;

    /**
     * \brief new node name
     */
    QLineEdit *m_name;
    
    /**
     * \brief new node template
     */
    QLineEdit *m_temp;

    /**
     * \brief new node base image
     */
    QLineEdit *m_mac;
   
    /**
     * \brief new node base image
     */
    QLineEdit *m_img;
   
    /**
     * \brief new node base image type
     */
    QLineEdit *m_imgType;
   
    /**
     * \brief new number of CPUs in node
     */
    QLineEdit *m_cpu;
   
    /**
     * \brief new size of Node's memory
     */
    QLineEdit *m_mem;
   
    /**
     * \brief Signal Mapper to send name to parent
     */
    QSignalMapper *m_signalMapper;
   
  public:
    /**
     * \brief Constructor of application gui
     * \param parent
     * \param dw
     * \param nodeIndex Node index
     */
    SettingsPage(QWidget *parent = 0, DragWidget *dw = 0, size_t nodeIndex = 0);

    /**
     * \brief clean and rebuild gui
     */
    void Refresh();

  public slots:

    /**
     * \brief save the node settings
     */
    void Save();
};


/**
 * \brief CyberVan Template page
 */
class TemplatePage : public QWidget
{
  Q_OBJECT

  signals:
    void labelUpdate();

  private:
    /**
     * \brief parent qwidget object
     */
    QWidget *m_parent;
    
    /**
     * \brief drag widget object
     */
    DragWidget *m_dw;
    
    /**
     * \brief selected Template index
     */
    int m_tempIndex;
    
    /**
     * \brief selected Template name
     */
    std::string m_tempName;
    
    /**
     * \brief selected Template CPU number
     */
    std::string m_tempCPU;
    
    /**
     * \brief selected Template memory amount
     */
    std::string m_tempMem;
    
    /**
     * \brief selected Template base image
     */
    std::string m_tempImg;

    /**
     * \brief selected Template base image type
     */
    std::string m_tempImgType;

    /**
     * \brief selected Template IP mask
     */
    std::string m_tempIPMask;

    /**
     * \brief new Template name
     */
    QLineEdit *m_name;
    
    /**
     * \brief new Template IP mask
     */
    QLineEdit *m_mask;
   
    /**
     * \brief new Template base image
     */
    QLineEdit *m_img;
   
    /**
     * \brief new Template base image type
     */
    QLineEdit *m_imgType;
   
    /**
     * \brief new number of CPUs
     */
    QLineEdit *m_cpu;
   
    /**
     * \brief new size of memory
     */
    QLineEdit *m_mem;
   
  public:
    /**
     * \brief Constructor of application gui
     * \param parent
     * \param dw
     * \param tempIndex Template index
     */
    TemplatePage(QWidget *parent = 0, DragWidget *dw = 0, int tempIndex = 0);

    /**
     * \brief clean and rebuild gui
     */
    void Refresh();

  public slots:

    /**
     * \brief save the template settings
     */
    void Save();

};


/**
 * \brief CyberVan Events page
 */
class EventPage : public QWidget
{
  Q_OBJECT

  private:
    /**
     * \brief parent qwidget object
     */
    QWidget *m_parent;
    
    /**
     * \brief drag widget object
     */
    DragWidget *m_dw;

    /**
     * \brief the events widget list
     */
    QTableWidget *tableWidget;
    
    /**
     * \brief selected Template index
     */
    int m_tempIndex;
    
  public:
    /**
     * \brief Constructor of application gui
     * \param parent
     * \param dw
     * \param tempIndex Template index
     */
    EventPage(QWidget *parent = 0, DragWidget *dw = 0, int tempIndex = 0);

    /**
     * \brief clean and rebuild gui
     */
    void Refresh();

  public slots:

    /**
     * \brief save the template settings
     */
    void Save();

    /**
     * \brief add row to for new event in event table
     */
    void addEventRow();
};

#endif /* SIMULATION_PAGES_H */