/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <github: juan->
 */

/**
 * \file settings-dialog.h
 * \brief Node Settings dialog window
 * \author Juan Sepulveda
 * \date 2016
 */

#ifndef SETTINGS_DIALOG_H
#define SETTINGS_DIALOG_H

#include <QDialog>
#include "drag-widget.h"
#include "simulation-pages.h"
#include "utils.h"

class QListWidget;
class QListWidgetItem;
class QStackedWidget;

/**
 * \brief Application dialog window class
 */
class SettingsDialog : public QDialog
{
  Q_OBJECT

  public:
    /**
     * \brief Application dialog constructor
     * \param dw
     */
    SettingsDialog(DragWidget *dw);
    
    /**
     * \brief drag widget object
     */
    DragWidget *m_dw;
    
    /**
     * \brief settings page object
     */
    SettingsPage *m_settings;

  public slots:
    
    /**
     * \brief quit
     */
    void quit();

    /**
     * \brief save and quit
     */
    void save();

    /**
     * \brief list interfaces
     */
    void bulkcheck(QTableWidgetItem * item);
    
  private:
    /**
     * \brief list interfaces
     */
    void listInterfaces();
    
    
    /**
     * \brief the content widget list
     */
    QTableWidget *tableWidget;
    
    /**
     * \brief the page widget list
     */
    QStackedWidget *pagesWidget;

    /**
     * \brief Number of Node Interfaces
     */
    std::vector<size_t> m_iFaceIndex;

    /**
     * \brief Initial IPs of interfaces
     */
    std::vector<std::string> IPs;

    /**
     * \brief Number of Node Interfaces
     */
    size_t m_iFaceNum;

    /**
     * \brief selected node index
     */
    size_t m_nodeIndex;
};

#endif /* SETTINGS_DIALOG_H */
