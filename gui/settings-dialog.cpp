/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <github: juan->
 */

/**
 * \file settings-dialog.cpp
 * \brief Node Settings dialog window
 * \author Juan Sepulveda
 * \date 2016
 */

#include <QtGui>

#include "settings-dialog.h"
#include "simulation-pages.h"
#include "main-window.h"
#include "utils.h"

SettingsDialog::SettingsDialog(DragWidget *dw)
{
  this->m_dw = dw;
  this->m_iFaceNum = 0;

  tableWidget = new QTableWidget(this);  

  pagesWidget = new QStackedWidget;


  size_t numNodes = this->m_dw->m_mw->GetGenerator()->GetNNodes();
  std::string m_nodeName = this->m_dw->GetSelected()->GetName();
  for(size_t i = 0; i < numNodes; i++)
  {
    if (m_nodeName == this->m_dw->m_mw->GetGenerator()->GetNode(i)->GetNodeName()) 
    {
      m_nodeIndex = i;
      break;
    }
  }
  

  this->m_settings = new SettingsPage(this, this->m_dw, m_nodeIndex);
  pagesWidget->addWidget(this->m_settings);
    

  QPushButton *closeButton = new QPushButton(tr("Close"));
  QPushButton *saveButton = new QPushButton(tr("Save"));

  listInterfaces();

  connect(closeButton, SIGNAL(clicked()), this, SLOT(quit()));
  connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));

  QHBoxLayout *horizontalLayout = new QHBoxLayout;
  horizontalLayout->addWidget(pagesWidget);
  horizontalLayout->addWidget(tableWidget, 1);

  QHBoxLayout *buttonsLayout = new QHBoxLayout;
  buttonsLayout->addStretch(3);
  buttonsLayout->addWidget(saveButton);
  buttonsLayout->addWidget(closeButton,1);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addLayout(horizontalLayout);
  mainLayout->addStretch(1);
  mainLayout->addSpacing(12);
  mainLayout->addLayout(buttonsLayout);
  setLayout(mainLayout);

  setWindowTitle(tr("Settings Dialog"));
}

void SettingsDialog::quit()
{
  emit close();
}
void SettingsDialog::save()
{
  // Save Node Properties
  this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetIpInterfaceNumber(m_iFaceNum);

  // Save Node Flags
  for (size_t i=0; i < m_iFaceNum; i++) {
    bool uniqueIp = false;
    QString mask = tableWidget->item(i+1,1)->text();
    QString mac = tableWidget->item(i+1,2)->text();
    std::string ip = tableWidget->item(i+1,0)->text().toStdString();
    std::string ipBase = ip.substr(0, ip.find_last_of("."));
    std::string currentBase = this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(m_iFaceIndex.at(i))->GetIpBase();
    
    // Input Check
    if (mask.toInt() > 32)
    {
      QMessageBox::about(this, "Error", "Please enter a valid mask value between 0 and 32");
      return;
    }

    // determine it's a unique ip
    if (ip.compare(IPs.at(i)) != 0)
      uniqueIp = true;

    if (uniqueIp) {
      this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetIP(i, ip);
      if (!mask.isEmpty())
      this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetMask(i, mask.toInt());
    } else 
      this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetIP(i, "");
    if (!mac.isEmpty())
      this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetMac(i, mac.toStdString());
    if (tableWidget->item(i+1,3)->checkState() == Qt::Unchecked) 
      this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->UnsetL2Flag(i);
    else
      this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetL2Flag(i);
  }
  this->m_settings->Save();
  //this->close();
}

void SettingsDialog::listInterfaces()
{
  QTableWidgetItem *newItem;
  std::string nodeName = this->m_dw->GetSelected()->GetName();

  for(size_t i = 0; i < this->m_dw->m_mw->GetGenerator()->GetNNetworkHardwares(); i++)
    {
      for(size_t j = 0; j < this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(i)->GetInstalledNodes().size(); j++)
      {
        std::vector<std::string> installedNodes =  this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(i)->GetInstalledNodes();
        if(installedNodes.at(j) == nodeName )
        {
          m_iFaceIndex.push_back(i);
          if (this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(i)->GetIpBase() != "") 
          {
            std::string base = this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(i)->GetIpBase();
            if ((j > 0) && ((installedNodes.at(0).find("ap_") == 0)))
            {
              IPs.push_back(base + "." + utils::integerToString(j));
              m_iFaceNum++;
            }
            else {
              if ((j > 1) && (installedNodes.at(j-2) == nodeName))
              {
                  m_iFaceIndex.push_back(i);
                  IPs.push_back(base + "." + utils::integerToString(j));
                  m_iFaceNum++;
              }
              IPs.push_back(base + "." + utils::integerToString(j + 1));
              m_iFaceNum++;
            }
          }
          else
          {
            if ((j > 0) && ((installedNodes.at(0).find("ap_") == 0)))
            {
              IPs.push_back("10.0."+ utils::integerToString(i) + "." + utils::integerToString(j));
              m_iFaceNum++;
            }
            else {
              if ((j > 1) && (installedNodes.at(j-2) == nodeName))
              {
                  m_iFaceIndex.push_back(i);
                  IPs.push_back("10.0."+ utils::integerToString(i) + "." + utils::integerToString(j));
                  m_iFaceNum++;
              }
              IPs.push_back("10.0."+ utils::integerToString(i) + "." + utils::integerToString(j + 1));
              m_iFaceNum++;
            }
          }
        }
      }
    }

  tableWidget->setMinimumWidth(290);
  //tableWidget->setMaximumWidth(219);
  tableWidget->setColumnCount(4);
  tableWidget->setRowCount(m_iFaceNum + 1);
  
  QStringList labels;
  labels << "Ip" << "Mask" << "MAC" << "L.2 Injection";
  tableWidget->setHorizontalHeaderLabels(labels);

  if (m_iFaceNum > 0) 
  {
    newItem = new QTableWidgetItem(tr("All"));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(0, 0, newItem); // Ip Col = 0
    newItem = new QTableWidgetItem(tr(""));
    tableWidget->setItem(0, 1, newItem);
    newItem = new QTableWidgetItem(tr(""));
    tableWidget->setItem(0, 2, newItem);
    QTableWidgetItem *checkAll = new QTableWidgetItem(tr(""));
    checkAll->setFlags(newItem->flags() | Qt::ItemIsUserCheckable); // set checkable flag
    checkAll->setCheckState(Qt::Unchecked);
    tableWidget->setItem(0, 3, checkAll);
    connect(tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)), this, SLOT(bulkcheck(QTableWidgetItem *)));
  }

  for (size_t i = 0; i < m_iFaceNum; i++) 
  {
    std::string ip = this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->GetIP(i);
    std::string mac = this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->GetMac(i);
    std::string mask = this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->GetMask(i);
    if (ip == "")
    {
      newItem = new QTableWidgetItem(tr(IPs.at(i).c_str()));
      newItem->setTextAlignment(Qt::AlignCenter); 
      tableWidget->setItem(i+1, 0, newItem); // Ip Col = 0
      newItem = new QTableWidgetItem(tr(""));
      newItem->setTextAlignment(Qt::AlignCenter); 
      tableWidget->setItem(i+1, 1, newItem);
    }
    else
    {
      newItem = new QTableWidgetItem(tr(ip.c_str()));
      newItem->setTextAlignment(Qt::AlignCenter); 
      tableWidget->setItem(i+1, 0, newItem);
      newItem = new QTableWidgetItem(tr(mask.c_str()));
      newItem->setTextAlignment(Qt::AlignCenter); 
      tableWidget->setItem(i+1, 1, newItem);
    }

    newItem = new QTableWidgetItem(tr(mac.c_str()));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(i+1, 2, newItem); 
    newItem = new QTableWidgetItem(tr(""));
    newItem->setFlags(newItem->flags() | Qt::ItemIsUserCheckable); // set checkable flag
    if (this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->IsSetL2Flag(i))
      newItem->setCheckState(Qt::Checked);
    else 
      newItem->setCheckState(Qt::Unchecked);
    tableWidget->setItem(i+1, 3, newItem);
  }

  // newItem = new QTab leWidgetItem(tr("%1").arg(
  //         (1)*(-1)));
  // tableWidget->setItem(2, 2, newItem);

}

void SettingsDialog::bulkcheck(QTableWidgetItem * item)
{
  if (item->column() != 3 || item->row() != 0) return;
  
  if (item->checkState() == Qt::Checked) 
    for (size_t i = 0; i < m_iFaceNum; i++)
    {
      tableWidget->item(i+1,item->column())->setCheckState(Qt::Checked);
    }
  else
    for (size_t i = 0; i < m_iFaceNum; i++)
    {
      tableWidget->item(i+1,item->column())->setCheckState(Qt::Unchecked);
    }
}
