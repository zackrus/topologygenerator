/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <github: juan->
 */

/**
 * \file simulation-pages.cpp
 * \brief Node Settings dialog window
 * \author Juan Sepulveda
 * \date 2016
 */

#include <QtGui>

#include "simulation-pages.h"
#include "main-window.h"


SettingsPage::SettingsPage(QWidget *parent, DragWidget *dw, size_t nodeIndex) : QWidget(parent)
{
  this->m_dw = dw;
  this->m_parent = parent;
  this->m_nodeIndex = nodeIndex;
  this->m_node = this->m_dw->GetSelected();
  this->Refresh();
 }

void SettingsPage::Refresh()
{
  for(size_t i = 0; i < (size_t)this->children().size(); i++)
  {
    delete this->children().at(i);
  }
  delete this->layout();

  /* Get node properties */
  m_nodeName = this->m_node->GetName();
  m_nodeTemp = this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->GetTemplate();
  m_nodeImg = this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->GetImg();
  m_nodeImgType = this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->GetImgType();
  m_nodeCPU = this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->GetCPU();
  m_nodeMem = this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->GetMem();

  if (m_nodeCPU == "0")
    m_nodeCPU = "";
  if (m_nodeMem == "0")
    m_nodeMem = "";


  /* Generate UI */
  QGroupBox *appsGroup = new QGroupBox(tr("Node Settings"));
 
  QGridLayout *layout = new QGridLayout;

  /* Name */
  QLabel *lname = new QLabel("Name:");
  m_name = new QLineEdit( QString(m_nodeName.c_str()) );
 
  layout->addWidget(lname, 2, 0);
  layout->addWidget(m_name, 2, 1);

  // Template
  QLabel *ltemp = new QLabel("Template:");
  m_temp = new QLineEdit(QString(m_nodeTemp.c_str()));
 
  layout->addWidget(ltemp,3, 0);
  layout->addWidget(m_temp, 3, 1);
 
  // OS
  QLabel *limg = new QLabel("Base Image:");
  m_img = new QLineEdit(QString(m_nodeImg.c_str()));

  layout->addWidget(limg,4, 0);
  layout->addWidget(m_img, 4, 1);
 
  // Image Type
  QLabel *limgType = new QLabel("Image Type:");
  m_imgType = new QLineEdit(QString(m_nodeImgType.c_str()));

  layout->addWidget(limgType,5, 0);
  layout->addWidget(m_imgType, 5, 1);
 
  // CPUs
  QLabel *lcpu = new QLabel("CPUs:");
  m_cpu = new QLineEdit(QString(m_nodeCPU.c_str()));
 
  layout->addWidget(lcpu,6, 0);
  layout->addWidget(m_cpu, 6, 1);

  // Memory
  QLabel *lmem = new QLabel("Memory:");
  m_mem = new QLineEdit(QString(m_nodeMem.c_str()));
 
  layout->addWidget(lmem,7, 0);
  layout->addWidget(m_mem, 7, 1);
 
  /* Add apps Button*/
  // QPushButton *addButton = new QPushButton(tr("Save"));
  // connect(addButton, SIGNAL( clicked() ), this, SLOT( Save() ) );
  
  /* layout ... */
  appsGroup->setLayout(layout);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(appsGroup);
  //mainLayout->addWidget(addButton);
  mainLayout->addStretch(1);
  setLayout(mainLayout);
}

void SettingsPage::Save()
{
  std::string name = m_name->text().toStdString();
  std::string temp = m_temp->text().toStdString();
  std::string img = m_img->text().toStdString();
  std::string imgType = m_imgType->text().toStdString();
  size_t cpu = m_cpu->text().toInt();
  size_t mem = m_mem->text().toInt();

  bool valid = true;
  for(size_t i = 0; i < this->m_dw->m_mw->GetGenerator()->GetNNodes(); i++)
  {
    if ((i != m_nodeIndex) && (name == this->m_dw->m_mw->GetGenerator()->GetNode(i)->GetNodeName())) 
    {
      valid = false;
      QMessageBox::about(this, "Error", "Node name already in use.");
    }
  }

  if (valid) {
    this->m_node->SetName(name);
    this->m_dw->RenameDrawLine(m_nodeName, name);
    this->m_dw->m_mw->GetGenerator()->RenameNode(m_nodeName, name);
    this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetNodeName(name);
    this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetTemplate(temp);
    this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetImg(img);
    this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetImgType(imgType);
    this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetCPU(cpu);
    this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetMem(mem);
    this->m_dw->UpdateToolTips();

    QMessageBox::about(this, "Settings", "Settings saved successfully");
    // close the application window
    this->m_parent->close();
  }

}

//
// Template Page
//
TemplatePage::TemplatePage(QWidget *parent, DragWidget *dw, int tempIndex) : QWidget(parent)
{
  this->m_dw = dw;
  this->m_parent = parent;
  this->m_tempIndex = tempIndex;
  //this->m_temp = this->m_dw->GetNodeSelected();
  this->Refresh();
 }

void TemplatePage::Refresh()
{
  for(size_t i = 0; i < (size_t)this->children().size(); i++)
  {
    delete this->children().at(i);
  }
  delete this->layout();

  /* Get template properties */
  if(this->m_tempIndex != -1) {
    m_tempName = this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->GetTempName();
    m_tempImg = this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->GetImg();
    m_tempImgType = this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->GetImgType();
    m_tempCPU = this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->GetCPU();
    m_tempMem = this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->GetMem();
    m_tempIPMask = this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->GetIPMask();
    if(m_tempCPU == "0")
      m_tempCPU = "";
    if(m_tempMem == "0")
      m_tempMem = "";
  } 
  else 
  {
    m_tempName = "";
    m_tempImg = "";
    m_tempImgType = "pvm";
    m_tempCPU = "";
    m_tempMem = "";
    m_tempIPMask = "24";
  }


  /* Generate UI */
  QGroupBox *appsGroup = new QGroupBox(tr("Template Settings"));
 
  QGridLayout *layout = new QGridLayout;

  /* Name */
  QLabel *lname = new QLabel("Name:");
  m_name = new QLineEdit( QString(m_tempName.c_str()) );
 
  layout->addWidget(lname, 2, 0);
  layout->addWidget(m_name, 2, 1);
 
  // OS
  QLabel *limg = new QLabel("Base Image:");
  m_img = new QLineEdit(QString(m_tempImg.c_str()));

  layout->addWidget(limg,3, 0);
  layout->addWidget(m_img, 3, 1);
 
  // Image Type
  QLabel *limgType = new QLabel("Image Type:");
  m_imgType = new QLineEdit(QString(m_tempImgType.c_str()));

  layout->addWidget(limgType,4, 0);
  layout->addWidget(m_imgType, 4, 1);
 
  // CPUs
  QLabel *lcpu = new QLabel("CPUs:");
  m_cpu = new QLineEdit(QString(m_tempCPU.c_str()));
 
  layout->addWidget(lcpu,5, 0);
  layout->addWidget(m_cpu, 5, 1);

  // Memory
  QLabel *lmem = new QLabel("Memory:");
  m_mem = new QLineEdit(QString(m_tempMem.c_str()));
 
  layout->addWidget(lmem,6, 0);
  layout->addWidget(m_mem, 6, 1);
 
  // IP Mask
  QLabel *lmask = new QLabel("IP Mask:");
  m_mask = new QLineEdit(QString(m_tempIPMask.c_str()));
 
  layout->addWidget(lmask,7, 0);
  layout->addWidget(m_mask, 7, 1);

  /* Save and Update Template names in menu*/
  QPushButton *saveButton = new QPushButton(tr("Save"));
  connect(saveButton, SIGNAL( clicked() ), this, SLOT( Save() ) );
  //connect(this, SIGNAL( labelUpdate() ), this->m_parent, SLOT( save() ) );

  /* layout ... */
  appsGroup->setLayout(layout);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(appsGroup);
  mainLayout->addWidget(saveButton, 1);
  mainLayout->addStretch(1);

  setLayout(mainLayout);
}

void TemplatePage::Save()
{
  std::string name = m_name->text().toStdString();
  std::string img = m_img->text().toStdString();
  std::string imgType = m_imgType->text().toStdString();
  size_t cpu = m_cpu->text().toInt();
  size_t mem = m_mem->text().toInt();
  size_t ipmask = m_mask->text().toInt();

  if (name == "")
  {
    QMessageBox::about(this, "Error", "Please enter a valid name");
    return;
  }

  if (ipmask > 32)
  {
    QMessageBox::about(this, "Error", "Please IP mask less than 32");
    return;
  }

  bool valid = true;
  int NTemp = (int) this->m_dw->m_mw->GetGenerator()->GetNTemplates();
  for(int i = 0; i < NTemp; i++)
  {
    if ((i != m_tempIndex) && (name == this->m_dw->m_mw->GetGenerator()->GetTemplate(i)->GetTempName())) 
    {
      valid = false;
      QMessageBox::about(this, "Error", "Node name already in use.");
    }
  }

  if (valid)
  {
    if(this->m_tempIndex != -1) 
    {
      this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->SetTempName(name);
      this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->SetImg(img);
      this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->SetImgType(imgType);
      this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->SetCPU(cpu);
      this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->SetMem(mem);
      this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->SetIPMask(ipmask);
    }
    else
    {
      this->m_tempIndex = this->m_dw->m_mw->GetGenerator()->GetNTemplates();
      this->m_dw->m_mw->GetGenerator()->AddTemplate(name, img, imgType, cpu, mem, ipmask);
    }

    emit labelUpdate();
  }
  return;
  // close the application window
  // this->m_parent->close();
}

//
// Event Page
//
EventPage::EventPage(QWidget *parent, DragWidget *dw, int tempIndex) : QWidget(parent)
{
  this->m_dw = dw;
  this->m_parent = parent;
  this->m_tempIndex = tempIndex;
  //this->m_temp = this->m_dw->GetNodeSelected();
  this->Refresh();
 }

void EventPage::Refresh()
{
  for(size_t i = 0; i < (size_t)this->children().size(); i++)
  {
    delete this->children().at(i);
  }
  delete this->layout();

  tableWidget = new QTableWidget(this);

  /* Evemts table */

  //tableWidget->setMinimumWidth(290);
  //tableWidget->setMaximumWidth(219);
  tableWidget->setColumnCount(2);
  tableWidget->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
  QStringList labels;
  labels << "Script" << "Start Time";
  tableWidget->setHorizontalHeaderLabels(labels);
  
  QTableWidgetItem *newItem;
  if(m_tempIndex == -1)
  {
    tableWidget->setRowCount(1);
    newItem = new QTableWidgetItem(tr(""));
    newItem->setTextAlignment(Qt::AlignCenter);
    tableWidget->setItem(0, 0, newItem);
    newItem = new QTableWidgetItem(tr(""));
    newItem->setTextAlignment(Qt::AlignCenter);
    tableWidget->setItem(0, 1, newItem);
  } else {
    size_t listSize = this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->GetNEvent();
    tableWidget->setRowCount(listSize + 1);
    for (size_t i = 0; i < listSize; i++) 
    {
      std::string startTime = this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->GetEvent(i)->GetTime();
      std::string scriptName = this->m_dw->m_mw->GetGenerator()->GetTemplate(m_tempIndex)->GetEvent(i)->GetScript();
      newItem = new QTableWidgetItem(tr(scriptName.c_str()));
      newItem->setTextAlignment(Qt::AlignCenter);
      tableWidget->setItem(i, 0, newItem);
      newItem = new QTableWidgetItem(tr(startTime.c_str()));
      newItem->setTextAlignment(Qt::AlignCenter); 
      tableWidget->setItem(i, 1, newItem);
    }
    newItem = new QTableWidgetItem(tr(""));
    newItem->setTextAlignment(Qt::AlignCenter);
    tableWidget->setItem(listSize + 1, 0, newItem);
    newItem = new QTableWidgetItem(tr(""));
    newItem->setTextAlignment(Qt::AlignCenter);
    tableWidget->setItem(listSize + 1, 1, newItem);
  }

  QPushButton *addButton = new QPushButton(tr("+"));
  connect(addButton, SIGNAL(clicked()), this, SLOT(addEventRow()));

  /* layout ... */
  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(tableWidget);
  mainLayout->addWidget(addButton, 1);
  mainLayout->addStretch(1);

  setLayout(mainLayout);
}

void EventPage::Save()
{
  int NTemp = (int) this->m_dw->m_mw->GetGenerator()->GetNTemplates();
  size_t index = m_tempIndex;
  if (m_tempIndex == -1) {
    index = NTemp - 1; 
    this->m_tempIndex = NTemp -1 ;
  }
  size_t listSize = this->m_dw->m_mw->GetGenerator()->GetTemplate(index)->GetNEvent();
  for (int i = listSize-1; i >= 0; i--) 
  {
    std::string scriptName = tableWidget->item(i,0)->text().toStdString();
    size_t startTime = tableWidget->item(i,1)->text().toInt();
    if (scriptName == "")
    {
      this->m_dw->m_mw->GetGenerator()->GetTemplate(index)->DeleteEvent(i);
    }
    else
    {
      this->m_dw->m_mw->GetGenerator()->GetTemplate(index)->GetEvent(i)->SetScript(scriptName);
      this->m_dw->m_mw->GetGenerator()->GetTemplate(index)->GetEvent(i)->SetTime(startTime);
    }
  }

  for (size_t i = listSize; i < (size_t) tableWidget->rowCount(); i++) 
  {
    if ((tableWidget->item(i,0) != 0) && (tableWidget->item(i,1) != 0)) 
    {
      bool ok;
      std::string scriptName = tableWidget->item(i,0)->text().toStdString();
      size_t startTime = tableWidget->item(i,1)->text().toInt(&ok);
      if ((scriptName != "") && (ok))
        this->m_dw->m_mw->GetGenerator()->GetTemplate(index)->NewEvent(scriptName, startTime);
    }
  }
  QMessageBox::about(this, "Template", "Template saved successfully");

// close the application window
// this->m_parent->close();
}


void EventPage::addEventRow()
{
  QTableWidgetItem *newItem = new QTableWidgetItem(tr(""));
  newItem->setTextAlignment(Qt::AlignCenter);
  tableWidget->setItem(tableWidget->rowCount(), 0, newItem);
  newItem = new QTableWidgetItem(tr(""));
  newItem->setTextAlignment(Qt::AlignCenter);
  tableWidget->setItem(tableWidget->rowCount(), 1, newItem);
  tableWidget->setRowCount(tableWidget->rowCount()+1);
}
