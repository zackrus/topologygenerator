/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Strasbourg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <juan-@gitlab>
 */

/**
 * \file template-dialog.cpp
 * \brief Template dialog window
 * \author Juan Sepulveda
 * \date 2016
 */

#include <QtGui>

#include "template-dialog.h"
#include "simulation-pages.h"
#include "main-window.h"

TemplateDialog::TemplateDialog(DragWidget *dw)
{
  this->m_dw = dw;
  contentsWidget = new QListWidget;
  contentsWidget->setViewMode(QListView::IconMode);
  contentsWidget->setMovement(QListView::Static);
  contentsWidget->setMaximumWidth(128);
  contentsWidget->setSpacing(12);

  pagesWidget = new QStackedWidget;
  eventsWidget = new QStackedWidget;
  

  size_t listSize = this->m_dw->m_mw->GetGenerator()->GetNTemplates();
  for(size_t i = 0; i < listSize; i++)
  {
    TemplatePage *tempPage = new TemplatePage(this, this->m_dw, i);
    EventPage *evePage = new EventPage(this, this->m_dw, i);
    pagesWidget->addWidget(tempPage);
    eventsWidget->addWidget(evePage);
    connect(tempPage, SIGNAL(labelUpdate()), this, SLOT(save()));
    connect(tempPage, SIGNAL(labelUpdate()), evePage, SLOT(Save()));
  }

  //this->m_template = new TemplatePage(this, this->m_dw, -1);
  TemplatePage *tempPage = new TemplatePage(this, this->m_dw, -1);
  EventPage *evePage = new EventPage(this, this->m_dw, -1);
  pagesWidget->addWidget(tempPage);
  eventsWidget->addWidget(evePage);
  connect(tempPage, SIGNAL(labelUpdate()), this, SLOT(save()));
  connect(tempPage, SIGNAL(labelUpdate()), evePage, SLOT(Save()));

  QPushButton *newButton = new QPushButton(tr("new"));
  QPushButton *delButton = new QPushButton(tr("delete"));
  QPushButton *closeButton = new QPushButton(tr("Close"));
  //QPushButton *addButton = new QPushButton(tr(" + "));


  createTempList();
  contentsWidget->setCurrentRow(0);

  connect(newButton, SIGNAL(clicked()), this, SLOT(newTemp()));
  connect(delButton, SIGNAL(clicked()), this, SLOT(delTemp()));
  connect(closeButton, SIGNAL(clicked()), this, SLOT(quit()));

  QHBoxLayout *horizontalLayout = new QHBoxLayout;
  horizontalLayout->addWidget(contentsWidget);
  horizontalLayout->addWidget(pagesWidget, 1);
  horizontalLayout->addWidget(eventsWidget, 2);

  QHBoxLayout *buttonsLayout = new QHBoxLayout;
  buttonsLayout->addStretch(3);
  buttonsLayout->addWidget(newButton);
  buttonsLayout->addWidget(delButton, 1);
  buttonsLayout->addWidget(closeButton, 2);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addLayout(horizontalLayout);
  mainLayout->addStretch(1);
  mainLayout->addSpacing(12);
  mainLayout->addLayout(buttonsLayout);
  setLayout(mainLayout);

  setWindowTitle(tr("Template Dialog"));
}

void TemplateDialog::save()
{
  // Update Template Names on the side
  size_t index = contentsWidget->currentRow();
  std::string name = this->m_dw->m_mw->GetGenerator()->GetTemplate(index)->GetTempName();
  contentsWidget->currentItem()->setText(QString(name.c_str()));
}

void TemplateDialog::quit()
{

  emit close();
}

void TemplateDialog::createTempList()
{
  size_t listSize = this->m_dw->m_mw->GetGenerator()->GetNTemplates();
  for(size_t i = 0; i < listSize; i++)
  {
    QListWidgetItem *tempButton = new QListWidgetItem(contentsWidget);
    tempButton->setText(QString(this->m_dw->m_mw->GetGenerator()->GetTemplate(i)->GetTempName().c_str()));
    tempButton->setTextAlignment(Qt::AlignHCenter);
    tempButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
  }
   QListWidgetItem *eventButton = new QListWidgetItem(contentsWidget);
   eventButton->setText(tr("New Template"));
   eventButton->setTextAlignment(Qt::AlignHCenter);
   eventButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
  
   connect(contentsWidget,
           SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
           this, SLOT(changePage(QListWidgetItem*,QListWidgetItem*)));
}

void TemplateDialog::changePage(QListWidgetItem *current, QListWidgetItem *previous)
{
  if (!current)
  {
    current = previous;
  }

  pagesWidget->setCurrentIndex(contentsWidget->row(current));
  eventsWidget->setCurrentIndex(contentsWidget->row(current));
}

void TemplateDialog::newTemp()
{
  QListWidgetItem *tempButton = new QListWidgetItem(contentsWidget);
  tempButton->setText(tr("New Template"));
  tempButton->setTextAlignment(Qt::AlignHCenter);
  tempButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
  TemplatePage *tempPage = new TemplatePage(this, this->m_dw, -1);
  EventPage *evePage = new EventPage(this, this->m_dw, -1);
  pagesWidget->addWidget(tempPage);
  eventsWidget->addWidget(evePage);
  connect(tempPage, SIGNAL(labelUpdate()), this, SLOT(save()));
  connect(tempPage, SIGNAL(labelUpdate()), evePage, SLOT(Save()));
}

void TemplateDialog::delTemp()
{
  QListWidgetItem *current = contentsWidget->currentItem();
  size_t index = contentsWidget->row(current);

  if (current->text() == "New Template")
  {
    QMessageBox::about(this, "Error", "Please select a valid template");
  }
  else
  {
    this->m_dw->m_mw->GetGenerator()->RemoveTemplate((current->text()).toStdString());
    QListWidgetItem *next = contentsWidget->item(index+1);
    if (next == 0)
    {
      QListWidgetItem *tempButton = new QListWidgetItem(contentsWidget);
      tempButton->setText(tr("New Template"));
      tempButton->setTextAlignment(Qt::AlignHCenter);
      tempButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
      pagesWidget->addWidget(new TemplatePage(this, this->m_dw, -1));
      eventsWidget->addWidget(new EventPage(this, this->m_dw, -1));
    }
    contentsWidget->setCurrentRow(0);
    contentsWidget->takeItem(index);
    pagesWidget->setCurrentIndex(0);
    pagesWidget->removeWidget(pagesWidget->widget(index)); 
    eventsWidget->setCurrentIndex(0);
    eventsWidget->removeWidget(eventsWidget->widget(index)); 
  }
}

